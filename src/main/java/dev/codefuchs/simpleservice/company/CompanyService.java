package dev.codefuchs.simpleservice.company;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    Company getById(Long id);

    List<Company> getAll();

    Company save(Company company);

    Company getByName(String name);
}
