package dev.codefuchs.simpleservice.company;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(value = PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Contact {
    @NotNull(message = "Contact's first name must not be null")
    @NotBlank(message= "Contact's first name must not be empty")
    private String firstName;
    @NotNull(message = "Contact's last name must not be null")
    @NotBlank(message= "Contact's last name must not be empty")
    private String lastName;
    @NotNull(message = "Contact's phone must not be null")
    @NotBlank(message= "Contact's phone must not be empty")
    private String phone;
}
