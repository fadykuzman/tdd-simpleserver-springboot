package dev.codefuchs.simpleservice.company;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.validation.annotation.Validated;

import static jakarta.persistence.GenerationType.AUTO;
import static jakarta.persistence.GenerationType.SEQUENCE;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "seq_company")
    @SequenceGenerator(name = "seq_company", allocationSize = 4, initialValue = 1)
    @JsonIgnore
    private Long id;
    @NotNull(message = "Name must not be null")
    @NotBlank(message = "Name must not be empty")
    @Pattern(regexp = "[a-zA-Z].*", message = "Name must be a text")
    private String name;
    @NotNull(message = "Address must not be null")
    @NotBlank(message = "Address must not be empty")
    private String address;
    @NotNull(message = "Phone must not be null")
    @NotBlank(message = "Phone must not be empty")
    private String phone;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "firstName", column = @Column(name = "contact_first_name")),
            @AttributeOverride(name = "lastName", column = @Column(name = "contact_last_name")),
            @AttributeOverride(name = "phone", column = @Column(name = "contact_phone"))
    })
    @Valid
    @NotNull(message = "Contact must not be null. Contact is an object with the required values are: first_name, last_name, phone")
    private Contact contact;
}
