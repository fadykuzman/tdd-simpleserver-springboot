package dev.codefuchs.simpleservice.company;

import com.google.common.base.CaseFormat;
import dev.codefuchs.simpleservice.company.exceptions.CompanyErrorResponse;
import dev.codefuchs.simpleservice.company.exceptions.CompanyNotFoundException;
import dev.codefuchs.simpleservice.company.exceptions.Violation;
import org.checkerframework.checker.units.qual.A;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.*;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
class CompanyControllerAdvice {

    @ResponseStatus(NOT_FOUND)
    ResponseEntity<CompanyErrorResponse> handleCompanyNotFoundException(CompanyNotFoundException ex){
        var errorResponse = CompanyErrorResponse.builder()
                .error(NOT_FOUND)
                .message(ex.getMessage())
                .build();
        return ResponseEntity.status(NOT_FOUND).body(errorResponse);
    }


    @ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseBody
    CompanyErrorResponse onHttpMediaTypeNotSupported(){

        return CompanyErrorResponse.builder()
                .error(UNSUPPORTED_MEDIA_TYPE)
                .message("Request body is missing or media type is unsupported")
                .build();
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    Map<String, List<String>> onMethodArgumentNotValidException(
            MethodArgumentNotValidException e) {
        var errors = new HashMap<String, List<String>>();

        for (var fieldError : e.getBindingResult().getFieldErrors()) {
            var fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldError.getField());
            List<String> errorList;

            if (!errors.containsKey(fieldName)) errorList = new ArrayList<>();
            else errorList = errors.get(fieldName);

            errorList.add(fieldError.getDefaultMessage());
            errors.put(fieldName, errorList);
        }
        return errors;
    }
}
