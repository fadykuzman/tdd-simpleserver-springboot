package dev.codefuchs.simpleservice.company.implementation;

import dev.codefuchs.simpleservice.company.Company;
import dev.codefuchs.simpleservice.company.CompanyAPI;
import dev.codefuchs.simpleservice.company.CompanyService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController implements CompanyAPI {

    @Autowired
    private CompanyService service;

    @Override
    public Company getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @Override
    public Company getByName(@RequestParam String name) {
        return service.getByName(name);
    }

    @Override
    public List<Company> getAll() {
        return service.getAll();
    }

    @Override
    public Long postANewCompany(@Valid @RequestBody Company company) {
        var saved = service.save(company);
        return saved.getId();
    }
}
