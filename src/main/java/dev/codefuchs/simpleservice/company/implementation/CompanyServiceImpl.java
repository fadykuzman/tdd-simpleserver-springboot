package dev.codefuchs.simpleservice.company.implementation;

import dev.codefuchs.simpleservice.company.Company;
import dev.codefuchs.simpleservice.company.CompanyRepository;
import dev.codefuchs.simpleservice.company.CompanyService;
import dev.codefuchs.simpleservice.company.exceptions.CompanyNotFoundException;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository repository;

    @Override
    public Company getById(Long id) {
        return repository.findById(id)
                .orElseThrow(
                        () ->  new CompanyNotFoundException(String.format("Company with id: %d cannot be found", id))
                );
    }

    @Override
    public List<Company> getAll() {
        if (!repository.findAll().iterator().hasNext())
            throw new CompanyNotFoundException("There are no companies registered");
        return (List<Company>) repository.findAll();
    }

    @Override
    public Company save(Company company) {
        return repository.save(company);
    }

    @Override
    public Company getByName(String name) {
        return repository.findByName(name)
                .orElseThrow(
                        () -> new CompanyNotFoundException(String.format("Company with name %s not found", name)));
    }
}
