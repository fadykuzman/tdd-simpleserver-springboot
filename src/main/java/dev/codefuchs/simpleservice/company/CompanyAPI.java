package dev.codefuchs.simpleservice.company;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("companies")
@ResponseBody
public interface CompanyAPI {

    @GetMapping(value = "/{id}", produces = "application/json")
    Company getById(@PathVariable Long id);

    @GetMapping(value = "q", produces = "application/json")
    Company getByName(@RequestParam String name);

    @GetMapping(produces = "application/json")
    List<Company> getAll();

    @PostMapping(consumes = "application/json")
    @ResponseBody
    Long postANewCompany(@Valid @RequestBody Company company);
}
