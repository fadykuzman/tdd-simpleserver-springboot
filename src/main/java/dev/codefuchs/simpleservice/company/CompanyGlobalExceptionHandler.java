package dev.codefuchs.simpleservice.company;

import dev.codefuchs.simpleservice.company.exceptions.CompanyErrorResponse;
import dev.codefuchs.simpleservice.company.exceptions.CompanyNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice(basePackages = "dev.codefuchs.simpleservice.company")
public class CompanyGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CompanyNotFoundException.class)
    ResponseEntity<CompanyErrorResponse> handleCompanyNotFoundException(CompanyNotFoundException exception, WebRequest request) {
        var errorResponse = CompanyErrorResponse
                .builder()
                .error(NOT_FOUND)
                .message(exception.getMessage()).build();
        return ResponseEntity
                .status(404)
                .body(errorResponse);
    }
}
