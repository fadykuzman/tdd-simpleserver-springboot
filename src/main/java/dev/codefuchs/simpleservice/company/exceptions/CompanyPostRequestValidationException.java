package dev.codefuchs.simpleservice.company.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class CompanyPostRequestValidationException extends RuntimeException{
    public CompanyPostRequestValidationException(String message) {
        super(message);
    }
}
