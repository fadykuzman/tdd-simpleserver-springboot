package dev.codefuchs.simpleservice.company.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record Violation(
        @JsonProperty("field_name") String fieldName,
        List<String> message) {

}
