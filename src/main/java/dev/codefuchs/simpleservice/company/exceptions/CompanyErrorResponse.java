package dev.codefuchs.simpleservice.company.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

@Data
@Builder
@AllArgsConstructor
public class CompanyErrorResponse {
    private HttpStatus error;
    private String message;
}
