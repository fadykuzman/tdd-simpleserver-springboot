package dev.codefuchs.simpleservice.architecture;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.properties.HasName;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import dev.codefuchs.simpleservice.company.CompanyAPI;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.core.importer.ImportOption.*;
import static com.tngtech.archunit.library.Architectures.onionArchitecture;

@AnalyzeClasses(
        packages = "dev.codefuchs.simpleservice.company",
        importOptions = {
                DoNotIncludeTests.class,
                DoNotIncludeJars.class
        })
public class CodeToInterfaceTest {

    @ArchTest
    static final ArchRule controllers_should_implement_api =
            classes().that().implement(HasName.Predicates.nameEndingWith("API"))
                    .should().haveSimpleNameEndingWith("Controller");

    @ArchTest
    static final ArchRule servicesImpl_should_implement_service_interface =
            classes().that().implement(HasName.Predicates.nameEndingWith("Service"))
                    .should().haveSimpleNameEndingWith("ServiceImpl")
                    .andShould().beAnnotatedWith(Service.class);
}
