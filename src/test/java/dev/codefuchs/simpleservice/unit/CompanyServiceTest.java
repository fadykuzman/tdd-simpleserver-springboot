package dev.codefuchs.simpleservice.unit;

import dev.codefuchs.simpleservice.company.Company;
import dev.codefuchs.simpleservice.company.CompanyService;
import dev.codefuchs.simpleservice.company.Contact;
import dev.codefuchs.simpleservice.company.exceptions.CompanyNotFoundException;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.*;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Sql(value = "/clear-data.sql", executionPhase = BEFORE_TEST_METHOD)
public class CompanyServiceTest {

    @Autowired
    private CompanyService serviceUnderTest;

    @Test
    @Sql("/happy-path-data.sql")
    public void given_company_with_id_exists_when_service_is_called_then_it_returns_the_company() {
        var softly = new SoftAssertions();
        var actualCompany = serviceUnderTest.getById(1L);

        softly.assertThat(actualCompany.getName()).as("Company name")
                .isEqualTo("CompuMe");
        softly.assertThat(actualCompany.getAddress()).as("Company address")
                .isEqualTo("London 1");

        softly.assertThat(actualCompany.getPhone()).as("Company Phone Number")
                .isEqualTo("111-222-333");

        softly.assertAll();
    }

    @Test
    @Sql("/happy-path-data.sql")
    void given_company_with_id_doesnot_exist_when_service_is_called_then_it_throws_a_CompanyNotFoundException() {
        assertThatThrownBy(
                () -> serviceUnderTest.getById(9332323L))
                .isInstanceOf(CompanyNotFoundException.class);
    }

    @Test
    @Sql("/happy-path-data.sql")
    void when_all_is_called_all_companies_in_the_database_should_be_returned() {
        var companies = serviceUnderTest.getAll();
        assertThat(companies.size()).isEqualTo(3);
    }

    @Test
    void given_there_are_no_data_when_all_companies_are_requested_a_not_found_method_should_be_thrown() {
        assertThatThrownBy(
                () -> serviceUnderTest.getAll()
        ).isInstanceOf(CompanyNotFoundException.class)
                .hasMessage("There are no companies registered");
    }

    @Test
    void save_a_company_to_database() {
        var contact = Contact.builder()
                .firstName("Fiver")
                .lastName("Half-a-tenner")
                .phone("555-111-555")
                .build();
        var expectedCompany = Company.builder()
                .name("Company no. 5")
                .address("5th Street, House 5")
                .phone("555-555-555")
                .contact(contact)
                .build();

        var savedCompany = serviceUnderTest.save(expectedCompany);

        var actualCompany = serviceUnderTest.getById(savedCompany.getId());
        assertThat(actualCompany).isEqualTo(expectedCompany);
    }

    @Test
    @Sql("/happy-path-data.sql")
    void find_by_name() {
        var company = serviceUnderTest.getByName("CompuMe");
        assertThat(company.getId()).isEqualTo(1L);
    }

    @Test
    void when_company_not_found_an_exception_is_thrown() {
        assertThatThrownBy(
                () -> serviceUnderTest.getByName("DoesntExist")
        ).isExactlyInstanceOf(CompanyNotFoundException.class).hasMessage("Company with name DoesntExist not found");
    }
}
