package dev.codefuchs.simpleservice.testdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;

public enum RequestBodyJsonFiles {
    NEW_COMPANY_CORRECT("file:src/test/resources/test-data/post-request/happy-path.json"),
    EMPTY("file:src/test/resources/test-data/post-request/empty.json"),
    EMPTY_CONTACT("file:src/test/resources/test-data/post-request/empty-contact.json"),
    BLANK("file:src/test/resources/test-data/post-request/blank.json"),
    WRONG_TYPE("file:src/test/resources/test-data/post-request/wrong-type.json");

    private DefaultResourceLoader loader = new DefaultResourceLoader();
    private String path;
    RequestBodyJsonFiles(String path) {
        this.path = path;
    }
    public String getPath() {
        return path;
    }

    public BodyInserter<?, ? super ClientHttpRequest> getResource() {
        var resource = loader.getResource(path);
        return BodyInserters.fromResource(resource);
    }
}
