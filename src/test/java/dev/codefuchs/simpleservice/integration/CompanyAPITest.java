package dev.codefuchs.simpleservice.integration;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.reactive.server.WebTestClient;

import static dev.codefuchs.simpleservice.testdata.RequestBodyJsonFiles.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.skyscreamer.jsonassert.JSONCompareMode.LENIENT;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Sql(value = "/clear-data.sql", executionPhase = BEFORE_TEST_METHOD)
public class CompanyAPITest {
    @Autowired
    private WebTestClient client;

    @Test
    @Sql("/happy-path-data.sql")
    void controller_can_retrieve_an_entry_by_id() {

        client
                .get()
                .uri("/companies/1")
                .exchange()
                .expectBody()
                .consumeWith(System.out::println)
                .jsonPath("$.name").isEqualTo("CompuMe")
                .jsonPath("$.address").isEqualTo("London 1")
                .jsonPath("$.phone").isEqualTo("111-222-333")
                .jsonPath("$.contact.first_name").isEqualTo("Guy")
                .jsonPath("$.contact.last_name").isEqualTo("Fuchs")
                .jsonPath("$.contact.phone").isEqualTo("999-000-888");
    }

    @Test
    @Sql("/happy-path-data.sql")
    void when_a_non_existing_id_is_queried_then_a_not_found_response_is_given() {
        var id = 8989898L;

        client
                .get()
                .uri("companies/" + id)
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.error").isEqualTo("NOT_FOUND")
                .jsonPath("$.message").isEqualTo(String.format("Company with id: %d cannot be found", id))
                .jsonPath("$.trace").doesNotExist();
    }

    @Test
    @Sql("/happy-path-data.sql")
    void when_all_companies_are_requested_a_list_should_be_returned() {
        client
                .get()
                .uri("/companies")
                .accept(APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(3);
    }

    @Test
    void given_there_are_no_data_when_all_companies_are_requested_a_not_found_method_should_be_thrown() {
        client
                .get()
                .uri("/companies")
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.error").isEqualTo("NOT_FOUND")
                .jsonPath("$.message").isEqualTo("There are no companies registered")
                .jsonPath("$.trace").doesNotExist();
    }

    @Test
    void when_sending_a_post_request_with_empty_body_a_bad_request_is_sent_back_with_empty_body_message() {
        client
                .post()
                .uri("/companies")
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .consumeWith(System.out::println)
                .jsonPath("$.error").isEqualTo("UNSUPPORTED_MEDIA_TYPE")
                .jsonPath("$.message").isEqualTo("Request body is missing or media type is unsupported");
    }

    @Test
    void when_sending_a_post_request_with_empty_json_a_bad_request_is_sent_back_with_listing_violations() throws JSONException {

        var responseBody = client
                .post()
                .uri("/companies")
                .contentType(APPLICATION_JSON)
                .body(EMPTY.getResource())
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .returnResult()
                .getResponseBody();

        var bodyString = new String(responseBody, UTF_8);

        JSONAssert.assertEquals(
                "{\"name\": [\"Name must not be null\", \"Name must not be empty\"]}",
                bodyString, LENIENT);
        JSONAssert.assertEquals(
                "{\"address\": [\"Address must not be null\", \"Address must not be empty\"]}",
                bodyString, LENIENT);
        JSONAssert.assertEquals(
                "{\"phone\": [\"Phone must not be null\", \"Phone must not be empty\"]}",
                bodyString, LENIENT);
        JSONAssert.assertEquals(
                "{\"contact\": [\"Contact must not be null. " +
                        "Contact is an object with the required values are: " +
                        "first_name, last_name, phone\"]}",
                bodyString, LENIENT);
    }

    @Test
    void when_contact_is_given_but_values_are_null_response_with_each_specific_contact_field_violation_is_given() throws JSONException {
        var responseBody =
                client
                        .post()
                        .uri("/companies")
                        .contentType(APPLICATION_JSON)
                        .body(EMPTY_CONTACT.getResource())
                        .exchange()
                        .expectStatus()
                        .isBadRequest()
                        .expectBody()
                        .returnResult().getResponseBody();

        var body = new String(responseBody, UTF_8);

        JSONAssert.assertEquals(
                "{\"contact.first_name\": " +
                        "[\"Contact's first name must not be null\", " +
                        "\"Contact's first name must not be empty\"]}",
                body, LENIENT);

        JSONAssert.assertEquals(
                "{\"contact.last_name\": " +
                        "[\"Contact's last name must not be null\", " +
                        "\"Contact's last name must not be empty\"]}",
                body, LENIENT);

        JSONAssert.assertEquals(
                "{\"contact.phone\": " +
                        "[\"Contact's phone must not be null\", " +
                        "\"Contact's phone must not be empty\"]}",
                body, LENIENT);
    }

    @Test
    void when_name_has_a_number_instead_of_string_a_response_is_sent_with_violation() throws JSONException {
        var bodyResponse =
                client
                        .post()
                        .uri("/companies")
                        .contentType(APPLICATION_JSON)
                        .body(WRONG_TYPE.getResource())
                        .exchange()
                        .expectStatus()
                        .isBadRequest()
                        .expectBody()
                        .returnResult().getResponseBody();

        var body = new String(bodyResponse, UTF_8);

        JSONAssert.assertEquals("{\"name\": [\"Name must be a text\"]}",
                body,
                LENIENT);
    }

    @Test
    void post_happy_path() {
        var id = client
                .post()
                .uri("/companies")
                .contentType(APPLICATION_JSON)
                .body(NEW_COMPANY_CORRECT.getResource())
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(Long.class).returnResult().getResponseBody();

        client
                .get()
                .uri("/companies/" + id)
                .accept(APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.name").isEqualTo("Company no. 5");
    }

    @Test
    @Sql("/happy-path-data.sql")
    void find_by_name() {
        client
                .get()
                .uri("/companies/q?name=CompuMe")
                .accept(APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .consumeWith(System.out::println)
                .jsonPath("$.name").isEqualTo("CompuMe")
                .jsonPath("$.phone").isEqualTo("111-222-333");
    }

    @Test
    void given_name_doesnot_exist_when_queried_by_name_sends_a_not_found_http_code() {
        client
                .get()
                .uri("/companies/q?name=NotFOund")
                .accept(APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isNotFound();
    }
}
